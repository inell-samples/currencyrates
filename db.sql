USE [localdb]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Cache] (
    [Id]                         NVARCHAR (449)     COLLATE SQL_Latin1_General_CP1_CS_AS NOT NULL,
    [Value]                      VARBINARY (MAX)    NOT NULL,
    [ExpiresAtTime]              DATETIMEOFFSET (7) NOT NULL,
    [SlidingExpirationInSeconds] BIGINT             NULL,
    [AbsoluteExpiration]         DATETIMEOFFSET (7) NULL
);


GO
CREATE NONCLUSTERED INDEX [Index_ExpiresAtTime]
    ON [dbo].[Cache]([ExpiresAtTime] ASC);
	
	
CREATE TABLE [dbo].[RequestsLog](
	[RowID] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
	[RequestFrom] [nvarchar](3) NOT NULL,
	[RequestTo] [nvarchar](3) NULL,
	[RequestDateTime] [datetime] NOT NULL,
	[Response] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[RequestsLog] ADD  CONSTRAINT [DF_RequestsLog_RowID]  DEFAULT (newid()) FOR [RowID]
GO

