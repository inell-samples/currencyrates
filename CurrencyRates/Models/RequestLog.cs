﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CurrencyRates.Models
{
    [Table("RequestsLog")]
    public class RequestLog
    {
        [Column("RowID"), Key]
        public Guid RowID { get; set; }

        [Column("RequestFrom"), Required]
        public string RequestFrom { get; set; }

        [Column("RequestTo")]
        public string RequestTo { get; set; }

        [Column("RequestDateTime"), Required]
        public DateTime RequestDateTime { get; set; }

        [Column("Response")]
        public string Response { get; set; }
    }
}
