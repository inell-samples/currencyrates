﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CurrencyRates.Models
{
    public class RateInfo
    {
        public string To { get; set; }
        public decimal Rate { get; set; }
        public DateTime? ExpireAt { get; set; }
    }
}
