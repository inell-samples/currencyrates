﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;


namespace CurrencyRates.Models
{
    public class ApplicationDbContext : DbContext
    {
        public virtual DbSet<RequestLog> RequestsLog { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options) { }
    }
}
