﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net.Http;

using CurrencyRates.Models;

namespace CurrencyRates.AppCode.Services
{
    /// <summary>
    /// Base class for HTTP data services providers
    /// </summary>
    public abstract class HttpService
    {
        public HttpClient Client { get; }

        public HttpService(HttpClient client)
        {
            this.Client = client;
        }

        /// <summary>
        /// Obtains response from HttpResponseMessage
        /// </summary>
        /// <param name="task"></param>
        /// <returns>String response or an Expection</returns>
        protected object getResponse(Task<HttpResponseMessage> task)
        {
            if (task.Status != TaskStatus.RanToCompletion)
            {
                // Take the deepest inner exception to obtain error source
                return getInnerExteption(task.Exception);
            }

            var response = task.Result.Content.ReadAsStringAsync().Result;

            // Ensure Success Status Code
            try
            {
                task.Result.EnsureSuccessStatusCode();
            }
            catch (HttpRequestException ex)
            {
                // Pack raw response text into the exception data list
                ex.Data.Add("Response", response);
                return ex;
            }

            return response;
        }

        /// <summary>
        /// Gets the deepest inner exception recursively
        /// </summary>
        /// <param name="ex">Inner exception object</param>
        /// <returns></returns>
        private Exception getInnerExteption(Exception ex)
        {
            return (ex.InnerException != null) ? getInnerExteption(ex.InnerException) : ex;
        }

        /// <summary>
        /// Retrieves rates for the specified currencies
        /// </summary>
        /// <param name="RatesRequest"><see cref="RatesRequest"/> instance</param>
        /// <returns><see cref="RatesResponse"/> instance</returns>
        public abstract Task<RatesResponse> GetRates(RatesRequest RatesRequest);
    }
}
