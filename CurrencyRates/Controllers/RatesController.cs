﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

using CurrencyRates.Models;
using CurrencyRates.AppCode.Services;

namespace CurrencyRates.Controllers
{
    [Route("[controller]")]
    public class RatesController : Controller
    {
        protected IRateService __ratesService;
        protected ApplicationDbContext __dbContext;

        public RatesController(ApplicationDbContext DbContext, IRateService RatesService)
        {
            this.__ratesService = RatesService;
            this.__dbContext = DbContext;
        }

        [HttpGet("{from?}/{to?}")]
        public async Task<IActionResult> Index(RatesRequest request)
        {
            if (request.From == null)
                return BadRequest("Specify params");

            var rates = await this.__ratesService.GetRates(request);

            if (rates == null)
                return BadRequest("Bad request");

            this.logRequest(rates);
            return Json(rates);
        }

        protected void logRequest(RatesResponse ratesResponse)
        {
            var requestLog = new RequestLog()
            {
                RequestFrom = ratesResponse.From,
                RequestTo = ratesResponse.Rates.Length == 1 ? ratesResponse.Rates.First().To : null,
                RequestDateTime = DateTime.Now,
                Response = JsonConvert.SerializeObject(ratesResponse)
            };

            this.__dbContext.Add(requestLog);
            this.__dbContext.SaveChanges();
        }
    }
}
