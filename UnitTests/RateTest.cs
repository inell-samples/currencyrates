﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Options;
using Microsoft.EntityFrameworkCore;

using NUnit.Framework;
using Moq;

using CurrencyRates.Controllers;
using CurrencyRates.Models;
using CurrencyRates.AppCode.Services;
using CurrencyRates.AppCode.Configuration;

namespace UnitTests
{
    [TestFixture]
    public class RateTest
    {
        ExchangeratesapiHttpService __exchangeratesapiHttpService;
        IDistributedCache __cache;
        IOptions<CacheSettings> __cacheSettings;

        RatesController controller;
        ApplicationDbContext dbContext;

        [SetUp]
        public void SetUp()
        {
            var optionsMoq = new Mock<IOptions<CacheSettings>>();
            optionsMoq.Setup(m => m.Value).Returns(new CacheSettings()
            {
                AbsoluteExpirationSpan = 1
            });
            this.__cacheSettings = optionsMoq.Object;

            this.__exchangeratesapiHttpService = new ExchangeratesapiHttpService(new System.Net.Http.HttpClient(), this.__cacheSettings);
            this.__cache = new Mock<IDistributedCache>().Object;

            // Use memory DB for testing
            this.dbContext = new ApplicationDbContext(new DbContextOptionsBuilder<ApplicationDbContext>().UseInMemoryDatabase(databaseName: "memorydb").Options);

            var rateService = new RatesService(this.__exchangeratesapiHttpService, this.__cache, this.__cacheSettings);
            this.controller = new RatesController(this.dbContext, rateService);
        }

        /// <summary>
        /// If /Rates returns a list of rates if {To} parameter is omitted;
        /// </summary>
        /// <returns></returns>
        [Test]
        public async Task Rate_SpecifiedFrom_ReturnsMany() {

            // Arrange

            // Act
            var result = await this.controller.Index(new RatesRequest() { From = "RUB" });

            // Assert
            Assert.That(() =>
            {
                if (result is JsonResult)
                {
                    var response = (((JsonResult)result).Value as RatesResponse);
                    return response.Rates.Length > 1;
                    
                }
                return false;
            });

        }

        /// <summary>
        /// If /Rates returns the one rate if {To} parameter is specified
        /// </summary>
        /// <returns></returns>
        [Test]
        public async Task Rate_SpecifiedFromTo_ReturnsOne() {

            // Arrange

            // Act
            var result = await this.controller.Index(new RatesRequest() { From = "RUB", To = "EUR" });

            // Assert
            Assert.That(() =>
            {
                if (result is JsonResult)
                {
                    var response = (((JsonResult)result).Value as RatesResponse);
                    return response.Rates.Length == 1;
                }
                return false;
            });
        }

        /// <summary>
        /// If /Rates returns <see cref="BadRequestObjectResult"/> if there is no parameters were specified;
        /// </summary>
        /// <returns></returns>
        [Test]
        public async Task Rate_SpecifiedNone_ReturnsBadRequest() {

            // Arrange

            // Act
            var result = await this.controller.Index(new RatesRequest());

            // Assert
            Assert.That(() => result is BadRequestObjectResult);
        }
    }
}
